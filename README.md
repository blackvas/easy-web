# Easy Web

Сайт создан в рамках изучения базового курса HTML и CSS в объединении "Создаём сами сайт" в Центре детско-юношеского технического творчества и информационных технологий Пушкинского района Санкт-Петербурга в 2017 году. Преподаватель Асмолов Анатолий Фёдорович.
<a href="https://cttit.ru/" target="_blank">ЦДЮТТИТ</a>

<img src="assets/Second_place.jpg">
<img src="assets/Third_place.jpg">
<img src="assets/Certificate.jpg">